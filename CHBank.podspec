Pod::Spec.new do |spec|
  spec.swift_version = '5.1'
  spec.name         = "CHBank"
  spec.version      = "0.1.3"
  spec.summary      = "CLHMBank Module"
  spec.description  = <<-DESC
  CLHMBank module
                   DESC

  spec.homepage     = "https://www.clhs.com.ua/"
  spec.license      = "MIT"
  # spec.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  spec.author             = { "clhs" => "info@clhs.com.ua" }
  spec.ios.deployment_target = '10.2'
  spec.platform = :ios, '10.2'
  spec.ios.vendored_frameworks = 'CHBank.framework'
  spec.source            = { :http => 'https://bitbucket.org/clhs/ios-sdk/raw/ef8c816f4175874b063a390514bfd0c0094c7d0c/CHBankSDK.zip' }

  spec.frameworks = "UIKit", "AVFoundation"
  spec.dependency "Firebase", "6.25.0"
#spec.static_framework = true 

end
